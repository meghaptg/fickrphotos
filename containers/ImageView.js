import React from 'react';
import { StyleSheet, Image, View, Dimensions, Platform } from 'react-native';

export default class ImageView extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={{
                        height: Dimensions.get('window').height - (44 + Platform.OS === 'ios' ? 20 : 25) ,
                        width: Dimensions.get('window').width,
                    }}
                    resizeMode="contain"
                    source={{ uri: "http://farm" + this.props.data.farm + ".static.flickr.com/" + this.props.data.server + "/" + this.props.data.id + "_" + this.props.data.secret + ".jpg" }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
});