import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, TextInput, Dimensions, Platform, FlatList, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import * as actions from '../redux/actions';
import { connect } from 'react-redux';

const imageDimensions = Dimensions.get('window').width / 5

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: ''
        }
    }

    _keyExtractor = (item, index) => item.id;

    _renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => Actions.photo({ data: item })}>
            <View style={{ padding: 3 }}>
                <Image
                    style={{ height: imageDimensions, width: imageDimensions }}
                    resizeMode="cover"
                    source={{ uri: "http://farm" + item.farm + ".static.flickr.com/" + item.server + "/" + item.id + "_" + item.secret + ".jpg" }}
                />
            </View>
        </TouchableOpacity>
    );

    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.textBox}
                    underlineColorAndroid="transparent"
                    placeholder="search for images"
                    onChange={() => this.props.resetPageNumber()}
                    onChangeText={(text) => {
                        this.setState({ searchText: text });
                        if (text && text.length) {
                            this.props.getImages(text, 1);
                        } else {
                            this.props.clearImages()
                        }
                    }}
                />
                {this.props.imagesArray ?
                    <FlatList
                        style={{ marginTop: 40 }}
                        data={this.props.imagesArray}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        numColumns={4}
                        ListFooterComponent={<View style={{ height: 20 }} />}
                        onEndReachedThreshold={1}
                        onEndReached={() => this.props.getImages(this.state.searchText, this.props.page + 1)}
                    /> : null
                }
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textBox: {
        height: 44,
        // position: 'absolute',
        top: Platform.OS === 'ios' ? 30 : 35,
        marginVertical: 10,
        padding: 5,
        width: Dimensions.get('window').width - 30,
        borderColor: 'black',
        borderWidth: 1,
    }
});

function mapStateToProps(state) {
    return {
        imagesArray: state.imagesArray,
        page: state.page
    };
}

export default connect(mapStateToProps, actions)(Home);
