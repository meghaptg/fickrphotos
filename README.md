# fickrPhotos

React-native mobile app, to search and view photos.

Here is the link to the detailed description : 
https://docs.google.com/document/d/1dnY2vu7bUm70yi1Pe9m_tO4e51QjrlPL-xSmkdEJmiU/edit

Project runs both on android and iOS.

Steps to run the project (using expo): 
1. Clone the project.
2. Get into the project folder and run the command "npm install" or "yarn"
3. To run the project on android run the command "react-native run-android" and to run it on iOS use the command "react-native run-ios"