import { combineReducers } from 'redux';

const initialArray = [];

imagesArray = (state = initialArray, action) => {
    switch (action.type) {
        case 'IMAGES_UPDATED':
            let newState = [...state];
            newState = newState.concat(action.payload);
            return newState;

        case 'CLEAR_IMAGES':
            return initialArray;

        default:
            return state
    }
};

const pageNumber = 0;
page = (state = pageNumber, action) => {
    switch (action.type) {
        case 'UPDATE_PAGE_NUMBER':
            return action.payload;

        case 'CLEAR_IMAGES':
        case 'RESET_PAGE_NUMBER':
            return pageNumber;

        default:
            return state
    }
};

export default combineReducers({
    imagesArray,
    page
});
