import axios from 'axios';

export const getImages = (text, page) => {
    const api = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736& format=json&nojsoncallback=1&safe_search=1&page=' + page + '&text=' + text;
    return dispatch => {
        axios.get(api)
            .then(function (response) {
                console.log(response.data);
                dispatch({
                    type: 'IMAGES_UPDATED',
                    payload: response.data.photos.photo
                });
                dispatch({
                    type: 'UPDATE_PAGE_NUMBER',
                    payload: page
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}
export const clearImages = () => {
    return {
        type: 'CLEAR_IMAGES'
    }
}
export const resetPageNumber = () =>{
    return {
        type: 'RESET_PAGE_NUMBER'
    }
}