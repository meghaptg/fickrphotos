import React, { Component } from 'react';
import { Modal } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './redux/reducers';
import { Router, Scene, Stack } from 'react-native-router-flux';
import Home from './containers/Home';
import ImageView from './containers/ImageView';

const store = createStore(
  reducer,
  applyMiddleware(
    thunk,
  )
);

export default class app extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Stack key="root" component={Modal}>
            <Scene key="home" initial hideNavBar component={Home} title="Home"/>
            <Scene key="photo" component={ImageView} title="Image"/>
          </Stack>
        </Router>
      </Provider>
    );
  }
};



// import { createStackNavigator } from 'react-navigation';
// import React from 'react';
// import Home from './Home';
// import ImageView from './ImageView';
// console.log('came back');

// let Scenes = createStackNavigator({
//   HomePage: { screen: Home },
//   ImageViewPage: { screen: ImageView },
// });
// export default class App extends React.Component {
//   render() {
//     return <Scenes />
//   }
// }



// import React from 'react';
// import { StyleSheet, View } from 'react-native';
// import { createStackNavigator } from 'react-navigation';
// import Home from './Home';
// import ImageView from './ImageView';



// // export default App;

// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <View style={{ flex: 1, backgroundColor: 'red' }} />
//         <Scenes/>
//         <Home />
//         <ImageView />
//         <View style={{ flex: 1, backgroundColor: 'yellow' }} />
//       </View>
//     );
//   }
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     //height: 200, width:300,
//     backgroundColor: 'blue',
//     // alignItems: 'center',
//     // justifyContent: 'center',
//   },
// });

// /*import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Open up App.js to start working on your app!</Text>
//         <Text>Changes you make will automatically reload.</Text>
//         <Text>Shake your phone to open the developer menu.</Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
// */